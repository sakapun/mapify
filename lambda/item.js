module.exports = [
	{
		"name": "Jazz Rap",
		"intentName": "jazzrap",
		"spotifyURI": "spotify:playlist:37i9dQZF1DX8Kgdykz6OKj",
		"samples": [
			"jazz rap",
			"jazzy rap",
			"ジャジーラップの再生",
			"ラップジャズ",
			"ジャズラップ",
			"ジャジーラップ"
		]
	},
	{
		"name": "Ambient Tempo",
		"intentName": "ambient",
		"spotifyURI": "spotify:playlist:37i9dQZF1E4vNLyBvXKGFc",
		"samples": [
			"もわっとしたやつ",
			"空間的なやつ",
			"アンビエント",
			"アンビエントテクノ"
		]
	},
	{
		"name": "アコースティックギター",
		"intentName": "FingarStyleGuitar",
		"spotifyURI": "spotify:playlist:1SdkzJysOdajUpMGvlcbqt",
		"samples": [
			"ソロギター",
			"アコースティックギター",
			"アコギ",
			"ギターソロ"
		]
	},
	{
		"name": "Lofi ミュージック",
		"intentName": "LoFiIntent",
		"spotifyURI": "spotify:playlist:37i9dQZF1DWWQRwui0ExPn",
		"samples": [
			"気だるい感じの",
			"ローハイ",
			"lohi",
			"lo-fi",
			"ローファイ"
		]
	},
	{
		"name": "Mid Tempo",
		"intentName": "MidTempoIntent",
		"spotifyURI": "spotify:playlist:5CMvAWTlDPdZnkleiTHyyo",
		"samples": [
			"ミドルテンポの",
			"いい感じの",
			"mid tempo",
			"ミッドテンポ"
		]
	},
	{
		"name": "Funky Music",
		"intentName": "FunkyIntent",
		"spotifyURI": "spotify:playlist:37i9dQZF1DX70TzPK5buVf",
		"samples": [
			"ディスコミュージック",
			"ファンクなの",
			"funky",
			"ファンク",
			"ファンキーなやつ"
		]
	},
	{
		"name": "自然音",
		"intentName": "NaturalMusic",
		"spotifyURI": "spotify:playlist:37i9dQZF1DWYFCj3KyU0r0",
		"samples": [
			"自然",
			"川の音",
			"雨の音"
		]
	},
	{
		"name": "ピアノ、コンセントレーション",
		"intentName": "PianoConcentration",
		"spotifyURI": "spotify:playlist:7hXPli2ds4vLmFiZ8wmMkz",
		"samples": [
			"ピアノ",
			"コンセントレーションピアノ",
			"集中するピアノ",
			"集中クラシック"
		]
	},
	{
		"name": "ジャズ",
		"intentName": "Jazz",
		"spotifyURI": "spotify:playlist:0xPN5Hf3vlvzH6Oowi8rNQ",
		"samples": [
			"集中ジャズ",
			"バーでかかるジャズ"
		]
	}
]