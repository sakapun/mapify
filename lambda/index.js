// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');
const Mapify = require('./mapify.js');
const globalSetting = require('./item.js');
// test2

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speakOutput = 'いい感じのBGMをspotifyから選曲できます。何があるかはヘルプか一覧と言ってください';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const IntentCreator = (setting) => {
    return {
        canHandle(handlerInput) {
            return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
                && Alexa.getIntentName(handlerInput.requestEnvelope) === setting.intentName;
        },
        async handle(handlerInput) {
            const token = handlerInput.requestEnvelope.context.System.user.accessToken;
            const mapify = new Mapify();
            const didUpdate = await mapify.updateToken(token).play(setting.spotifyURI);
            if(didUpdate) {
                return handlerInput.responseBuilder
                    .speak(`${setting.name} を 再生します`)
                    //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
                    .withShouldEndSession(true)
                    .getResponse();
            } else {
                return handlerInput.responseBuilder
                    .speak(`このスキルではspotifyのオンはできません。先にSpotifyを再生してください`)
                    //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
                    .withShouldEndSession(true)
                    .getResponse();
            }

        }
    }
};

const VolumeHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
          && Alexa.getIntentName(handlerInput.requestEnvelope) === 'VolumeIntent';
    },
    async handle(handlerInput) {
        const token = handlerInput.requestEnvelope.context.System.user.accessToken;
        const mapify = new Mapify();
        const volume = Number(Alexa.getSlotValue(handlerInput.requestEnvelope, 'volume'))
        if (isFinite(volume) && volume <= 0) {
            return handlerInput.responseBuilder
            .speak(`ボリュームの設定が間違っています`)
              //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
              .withShouldEndSession(true)
              .getResponse();
        }
        const didUpdate = await mapify.updateToken(token).setVolume(volume);
        if(didUpdate) {
            return handlerInput.responseBuilder
            .speak(`${volume} にしました`)
              //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
              .withShouldEndSession(true)
              .getResponse();
        } else {
            return handlerInput.responseBuilder
            .speak(`このスキルではspotifyのオンはできません。先にSpotifyを再生してください`)
              //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
              .withShouldEndSession(true)
              .getResponse();
        }

    }
}

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speakOutput = "一覧です。" + globalSetting.map(g => g.name).join("、") ;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent'
                || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speakOutput = 'またね';
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .getResponse();
    }
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `${intentName} は登録されていません。どのジャンルにしますか？`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.stack}`);
        const speakOutput = `すいません、動作できませんでした。`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .getResponse();
    }
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        ...globalSetting.map(s => IntentCreator(s)),
        VolumeHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    .lambda();
