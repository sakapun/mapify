const request = require('request');

class Mapify {
  constructor () {
    this.token = '';
  }

  updateToken (token) {
    this.token = token;
    return this;
  }

  buildOptions () {
    return {
      headers: {
        'Authorization': `Bearer ${this.token}`,
        'Content-Type': 'application/json',
      },
      body: {
        text: 'Hello!'
      },
      json: true,
    }
  }

  postRequest (url, options) {
    return new Promise((resolve) => {
      request.post(
        url,
        options,
        (err, httpResponse, body) => {
          resolve({
            err: err,
            httpResponse: httpResponse,
            body: body
          });
        }
      );
    });
  };

  putRequest (url, options) {
    return new Promise((resolve) => {
      request.put(
        url,
        options,
        (err, httpResponse, body) => {
          resolve({
            err: err,
            httpResponse: httpResponse,
            body: body
          });
        }
      );
    });
  };

  getRequest (url) {
    return new Promise((resolve) => {
      request.get(
        url,
        {
          auth: {
            'bearer': this.token
          }
        },
        (err, httpResponse, body) => {
          resolve({
            err: err,
            httpResponse: httpResponse,
            body: body
          });
        }
      );
    });
  };

  async play (spotifyUri) {
    const options = {
      ...this.buildOptions(),
      body: {
        context_uri: spotifyUri
      }
    };
    const res = await this.putRequest(`https://api.spotify.com/v1/me/player/play`, options);
    if (res.httpResponse && res.httpResponse.statusCode === 404) {
      return false
    } else {
      return true
    }
  }

  async setVolume (volume) {
    const options = {
      ...this.buildOptions(),
      body: {
      }
    };
    const res = await this.putRequest(`https://api.spotify.com/v1/me/player/volume?volume_percent=${volume}`, options);
    if (res.httpResponse && res.httpResponse.statusCode === 404) {
      return false
    } else {
      return true
    }
  }
}

// const mapy = new Mapify();
// mapy.updateToken(token).play("spotify:album:5ht7ItJgpBH7W6vJ5BqpPr").then(r => console.log(r))

module.exports = Mapify;
