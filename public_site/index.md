# 利用規約

1. 本Alexa SkillではSpotifyのプレミアムアカウントが必須となります。  

2. 本アプリではSpotifyにログインしていただく必要があります。

3. 本アプリではご利用ユーザーのSpotifyアカウント情報を保管しておりません。  
APIの利用はSpotify社のライセンスによるものです
