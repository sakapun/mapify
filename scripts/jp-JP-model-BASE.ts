// このファイルを変更してもビルドしないと反映されないので注意！

export type Slot = {
  name: string,
  type: string,
}

export type Intent = {
  name: string,
  slots?: Slot[],
  samples: string[],
}

export type Model = {
  interactionModel: {
    languageModel: {
      invocationName: string,
      intents: Intent[],
      types: []
    }
  },
  version: string,
}

// このファイルを変更してもビルドしないと反映されないので注意！
export const model: Model =  {
  "interactionModel": {
    "languageModel": {
      "invocationName": "ジャンルbgm",
      "intents": [
        {
          "name": "AMAZON.CancelIntent",
          "samples": []
        },
        {
          "name": "AMAZON.HelpIntent",
          "samples": [
            "おすすめ",
            "何がおすすめ",
            "何が聞けるの",
            "一覧",
            "一覧を教えて"
          ]
        },
        {
          "name": "AMAZON.StopIntent",
          "samples": []
        },
        {
          "name": "AMAZON.NavigateHomeIntent",
          "samples": []
        },
        {
          "name": "VolumeIntent",
          "slots": [
            {
              "name": "volume",
              "type": "AMAZON.NUMBER"
            }
          ],
          "samples": [
            "{volume} にして",
            "ボリューム {volume}",
            "音の大きさ {volume}",
            "{volume}",
            "音量 {volume}"
          ]
        }
      ],
      "types": []
    }
  },
  "version": "11"
};

