export const skillJson: BaseSkillType = {
  "manifest": {
    "apis": {
      "custom": {
        "endpoint": {
          "uri": "arn:aws:lambda:us-east-1:638038257377:function:de478428-d201-4e97-a944-4ac839f00643:Release_0"
        },
        "interfaces": []
      }
    },
    "manifestVersion": "1.0",
    "privacyAndCompliance": {
      "locales": {
        "ja-JP": {
          "privacyPolicyUrl": "https://genrebgm.netlify.com/privacy",
          "termsOfUseUrl": "https://genrebgm.netlify.com/"
        }
      },
      "allowsPurchases": false,
      "usesPersonalInfo": false,
      "isChildDirected": false,
      "isExportCompliant": true,
      "containsAds": false
    },
    "publishingInformation": {
      "locales": {
        "ja-JP": {
          "name": "ジャンルBGM on Spotify",
          "smallIconUri": "https://s3.amazonaws.com/CAPS-SSE/echo_developer/3724/895ce949e34e4b20bd9b1d8f79c1a387/APP_ICON?versionId=iAZJvhAX7KtrYW8MdTzS9Z7Y5Cx7DCi_&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200415T141008Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAWBV6LQ4QDXWFVRR3%2F20200415%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=402eb05643136c05538af1fd2df2de89bba5def6ab74b69f7b1359580a0dd3f9",
          "largeIconUri": "https://s3.amazonaws.com/CAPS-SSE/echo_developer/5d14/10435ede7dcc4aa8852b96144837335c/APP_ICON_LARGE?versionId=3W23pN5LC2pHJ9vmQKWz6Ou3TzgctbcQ&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200415T141008Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAWBV6LQ4QDXWFVRR3%2F20200415%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=692aaa9e09d67c0568c3c4dc87e7a524d631a4303faaf1c555c13f239f8c81b9",
          "summary": "SpotifyでBGMに適したプレイリストを選曲できます。どんな呼び方でどんなプレイリストが選曲できるかをHPで公開しているので、曖昧さを回避できます。",
          "examplePhrases": [
            "Alexa、ジャンルBGMを開いて",
            "Alexa、ジャンルBGMでファンキーなやつを開いて",
            "Alexa、ジャンルBGMで気だるい感じのやつをかけて"
          ],
          "keywords": [
            "spotify",
            "music",
            "BGM",
            "spotifyジャンル",
            "spotifyでBGM"
          ],
        }
      },
      "isAvailableWorldwide": true,
      "distributionMode": "PUBLIC",
      "testingInstructions": "spotifyのAPIを使っているので、Spotifyのプレミアムアカウントが必要です。\nアカウントのユーザー情報をアカウントリンクで結びつけます。",
      "category": "MUSIC_INFO_REVIEWS_AND_RECOGNITION_SERVICE",
      "distributionCountries": [],
      "automaticDistribution": {
        "isActive": false
      }
    }
  }
}

export type BaseSkillType = {
  manifest: {
    apis: {
      custom: {
        endpoint: {
          uri: string,
        }
        interfaces: []
      },
    }
    "manifestVersion": "1.0",
    "privacyAndCompliance": {
      "locales": {
        "ja-JP": {
          "privacyPolicyUrl": "https://genrebgm.netlify.com/privacy",
          "termsOfUseUrl": "https://genrebgm.netlify.com/"
        }
      },
      "allowsPurchases": false,
      "usesPersonalInfo": false,
      "isChildDirected": false,
      "isExportCompliant": true,
      "containsAds": false
    },
    "publishingInformation": {
      "locales": {
        "ja-JP": {
          "name": string,
          "smallIconUri": string,
          "largeIconUri": string,
          "summary": string,
          "examplePhrases": string[],
          "keywords": string[],
        }
      },
      "isAvailableWorldwide": true,
      "distributionMode": string,
      "testingInstructions": string,
      "category": string,
      "distributionCountries": [],
      "automaticDistribution": {
        "isActive": false
      }
    }
  },
}

export type SkillJsonType = {
  manifest: {
    apis: {
      custom: {
        endpoint: {
          uri: string,
        }
        interfaces: []
      },
    }
    "manifestVersion": "1.0",
    "privacyAndCompliance": {
      "locales": {
        "ja-JP": {
          "privacyPolicyUrl": "https://genrebgm.netlify.com/privacy",
          "termsOfUseUrl": "https://genrebgm.netlify.com/"
        }
      },
      "allowsPurchases": false,
      "usesPersonalInfo": false,
      "isChildDirected": false,
      "isExportCompliant": true,
      "containsAds": false
    },
    "publishingInformation": {
      "locales": {
        "ja-JP": {
          "name": string,
          "smallIconUri": string,
          "largeIconUri": string,
          "summary": string,
          description: string,
          "examplePhrases": string[],
          "keywords": string[],
          "updatesDescription": string
        }
      },
      "isAvailableWorldwide": true,
      "distributionMode": string,
      "testingInstructions": string,
      "category": string,
      "distributionCountries": [],
      "automaticDistribution": {
        "isActive": false
      }
    }
  },
}


