import axios from 'axios';
import * as fs from 'fs';

const gitlabUrl = "https://gitlab.com/api/v4/projects/17774549";

type CommitData = {
  branch: string,
  commit_message: string,
  actions: CommitAction[]
}
type CommitAction = {
  action: "update",
  file_path: string,
  content: string,
}

const MODEL_PATH = "models/ja-JP.json";
const ITEM_PATH = "lambda/item.js";
const SKILL_PATH = "skill.json";


async function gitlabCommit(branch: string) {
  const modelContent = fs.readFileSync(__dirname+ '/../' + MODEL_PATH).toString();
  const itemContent = fs.readFileSync(__dirname+ '/../' + ITEM_PATH).toString();
  const skillContent = fs.readFileSync(__dirname+ '/../' + SKILL_PATH).toString();

  const commitData: CommitData = {
    "branch": branch,
    "commit_message": "auto update ci",
    "actions": [
      {
        "action": "update",
        "file_path": MODEL_PATH,
        "content": modelContent
      },
      {
        "action": "update",
        "file_path": ITEM_PATH,
        "content": itemContent
      },
      {
        "action": "update",
        "file_path": SKILL_PATH,
        "content": skillContent
      }
    ]
  };

  const gitlabCommitApi = gitlabUrl + "/repository/commits";
  await axios.post(gitlabCommitApi, commitData, {
    headers: {
      "PRIVATE-TOKEN": process.env.GITLAB_PRIVATE_TOKEN
    }
  });
}

async function gitlabCreateBranch(branch: string) {
  const gitlabBranchApi = gitlabUrl + `/repository/branches?branch=${branch}&ref=dev`;
  await axios.post(gitlabBranchApi, null, {
    headers: {
      "PRIVATE-TOKEN": process.env.GITLAB_PRIVATE_TOKEN
    }
  }).catch(e => {
    e.response.data.message
  });
}

async function gitlabCreateMr(branch: string) {
  const gitlabMrApi = gitlabUrl + `/merge_requests`;
  const mrData = {
    source_branch: branch,
    target_branch: "dev",
    title: "Data Update Request",
    remove_source_branch: true,
  };
  await axios.post(gitlabMrApi, mrData, {
    headers: {
      "PRIVATE-TOKEN": process.env.GITLAB_PRIVATE_TOKEN
    }
  }).catch(e => {
    console.log(e.response.data.message);
  });
}

async function main() {
  const branch = "auto_update";
  await gitlabCreateBranch(branch);
  await gitlabCommit(branch);
  await gitlabCreateMr(branch);
}


main();
