import axios from 'axios';
import {apiType} from "./api_type";
import * as fs from 'fs';
import {Intent, Model, model} from "./jp-JP-model-BASE";
import {skillJson, SkillJsonType} from "./skill-BASE";

const gasUrl = "https://script.google.com/macros/s/AKfycbzVIVMSm8MPFgkQKPBE6AG61zXdR9atGVXp2XqR230dpSkgJtQ/exec";
async function main()  {
  const res = await axios.get(gasUrl).then(r => r.data as apiType);
  writeSpotifyItems(res);
  writeModel(res);
  writeSkillJson(res);
  return;
}

function writeSpotifyItems (response: apiType) {
  const exportsString = "module.exports = ";
  const contentPage = response.contentsPage;
  const itemData = contentPage.map( item => {
    const samples = response.samplesPage.filter(s => s.intentName === item.intentName).map( s => {
      return s.sample;
    });
    return {
      ...item,
      samples
    };
  });
  const stringify = JSON.stringify(itemData, null, "\t");
  fs.writeFileSync(__dirname + '/../lambda/item.js', exportsString + stringify);
}

function writeModel (response: apiType) {
  const contents = response.contentsPage;
  const samples = response.samplesPage;
  const intents: Intent[] = contents.map(c => {
    return {
      name: c.intentName,
      slots: [],
      samples: samples.filter(s => s.intentName === c.intentName).map(s => s.sample),
    }
  });
  const nextModel: Model = {
    interactionModel: {
      languageModel: {
        ...model.interactionModel.languageModel,
        intents: [
          ...model.interactionModel.languageModel.intents,
          ...intents
        ]
      }
    },
    version: "12"
  };
  // console.log(nextModel);
  fs.writeFileSync(
    __dirname + '/../models/ja-JP.json',
    JSON.stringify(nextModel, null, "  ")
  );
}

function writeSkillJson(res: apiType) {
  const contentPage = res.contentsPage;
  const itemData = contentPage.map( item => {
    const samples = res.samplesPage.filter(s => s.intentName === item.intentName).map( s => {
      return s.sample;
    });
    return {
      ...item,
      samples
    };
  });
  const descList = itemData.map( item => {
    const sampleList = item.samples.map(s => " " + s).join("\n")
    return `【${item.name}】：\n` + sampleList
  }).join("\n\n");

  const description = "利用にはSpotifyプレミアムへの登録が必須です。\n\nプレイリスト一覧は利用規約に一覧化されています。\n\n" +
    descList;

  const updatesDescription = "HPを公開しました。ジャンルの公開はこれからです。";

  const newSkillJson: SkillJsonType = {
    ...skillJson,
    manifest: {
      ...skillJson.manifest,
      "publishingInformation": {
        ...skillJson.manifest.publishingInformation,
        "locales": {
          ...skillJson.manifest.publishingInformation.locales,
          "ja-JP": {
            ...skillJson.manifest.publishingInformation.locales["ja-JP"],
            description: description,
            updatesDescription
          }
        }
      }
    }
  };
  fs.writeFileSync(
    __dirname + '/../skill.json',
    JSON.stringify(newSkillJson, null, "  ")
  );
}

main();
