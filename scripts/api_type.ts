export type content = {
  name: string,
  intentName: string,
  spotifyURI: string,
}

export type intentSample = {
  intentName: string,
  sample: string,
}

export type apiType = {
  contentsPage: content[],
  samplesPage: intentSample[],
}
